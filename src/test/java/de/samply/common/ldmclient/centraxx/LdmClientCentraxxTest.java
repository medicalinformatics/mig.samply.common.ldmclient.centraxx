/*
 * Copyright (C) 2017 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.common.ldmclient.centraxx;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeFalse;

import com.google.common.base.Stopwatch;
import de.samply.common.ldmclient.LdmClient;
import de.samply.common.ldmclient.LdmClientUtil;
import de.samply.share.model.ccp.QueryResult;
import de.samply.share.model.query.common.Error;
import de.samply.share.model.query.common.View;
import de.samply.share.model.queryresult.common.QueryResultStatistic;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.apache.commons.io.IOUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

/**
 * Test Class for the LDM Connector for Centraxx Backends
 */
public class LdmClientCentraxxTest {

  private static final Logger logger = LoggerFactory.getLogger(LdmClientCentraxxTest.class);
  private static final boolean CACHING = true;

  private static final String centraxxBaseUrl = "http://localhost:9090/centraxx/";

  // In order to avoid unnecessary creation of too many requests...use a pre-existing result id from centraxx. Check the queryresults folder on the centraxx machine
  private static final String predefinedResultId = "99bdb78a-4cd4-418b-b4de-95dcc331e83f";
  private static final String predefinedResultLocation =
      centraxxBaseUrl + LdmClientCentraxx.REST_PATH_TEILER + LdmClientCentraxx.REST_PATH_REQUESTS
          + "/" + predefinedResultId;

  // Same for errors
  private static final String predefinedErrorResultId = "0ebf8ebb-0132-46f6-aaf6-f682682279dd";
  private static final String predefinedErrorResultLocation =
      centraxxBaseUrl + LdmClientCentraxx.REST_PATH_TEILER + LdmClientCentraxx.REST_PATH_REQUESTS
          + "/" + predefinedErrorResultId;

  private static final String VIEW_FILENAME_OK = "exampleView.xml";

  private LdmClient ldmClientCentraxx;
  private CloseableHttpClient httpClient;

  @Before
  public void setUp() throws Exception {
    this.httpClient = HttpClients.createDefault();
    this.ldmClientCentraxx = new LdmClientCentraxx(httpClient, centraxxBaseUrl, CACHING);
  }

  @After
  public void tearDown() throws Exception {
    httpClient.close();
    ((LdmClientCentraxx) ldmClientCentraxx).cleanQueryResultsCache();
  }

  // Since postView calls postViewString, just run the test once
  @Test
  public void testPostView() throws Exception {
    String viewString = readXmlFromFile(VIEW_FILENAME_OK);
    View view = xmlToView(viewString);
    String location = ldmClientCentraxx.postView(view);
    assertFalse(LdmClientUtil.isNullOrEmpty(location));
  }

  @Test
  public void testGetResult() throws Exception {
    assumeFalse(LdmClientUtil.isNullOrEmpty(predefinedResultId));
    assertTrue(ldmClientCentraxx.getResult(predefinedResultLocation) instanceof QueryResult);
  }

  /**
   * Test caching speed gain
   */
  @Test
  public void testGetResultPageThreeTimes() throws Exception {
    assumeFalse(LdmClientUtil.isNullOrEmpty(predefinedResultId));
    Stopwatch stopwatch = Stopwatch.createStarted();
    assertTrue(ldmClientCentraxx.getResultPage(predefinedResultLocation, 0) instanceof QueryResult);
    logger.info("Getting page 0 took " + stopwatch.stop().elapsed(TimeUnit.MILLISECONDS) + " ms");
    stopwatch.reset().start();
    assertTrue(ldmClientCentraxx.getResultPage(predefinedResultLocation, 1) instanceof QueryResult);
    logger.info("Getting page 1 took " + stopwatch.stop().elapsed(TimeUnit.MILLISECONDS) + " ms");
    stopwatch.reset().start();
    assertTrue(ldmClientCentraxx.getResultPage(predefinedResultLocation, 0) instanceof QueryResult);
    logger.info("Getting page 0 took " + stopwatch.stop().elapsed(TimeUnit.MILLISECONDS) + " ms");
  }

  @Test
  public void testGetResultPage() throws Exception {
    assumeFalse(LdmClientUtil.isNullOrEmpty(predefinedResultId));
    assertTrue(ldmClientCentraxx.getResultPage(predefinedResultLocation, 0) instanceof QueryResult);
  }

  @Test
  public void testGetStatsOrError() throws Exception {
    assumeFalse(LdmClientUtil.isNullOrEmpty(predefinedResultId) && LdmClientUtil
        .isNullOrEmpty(predefinedErrorResultId));
    Object statsOrError = ldmClientCentraxx.getStatsOrError(predefinedResultLocation);
    assertTrue((statsOrError instanceof QueryResultStatistic) || (statsOrError instanceof Error));
  }

  @Test
  public void testGetQueryResultStatistic() throws Exception {
    assumeFalse(LdmClientUtil.isNullOrEmpty(predefinedResultId));
    assertTrue(ldmClientCentraxx
        .getQueryResultStatistic(predefinedResultLocation) instanceof QueryResultStatistic);
  }

  @Test
  public void testGetError() throws Exception {
    assumeFalse(LdmClientUtil.isNullOrEmpty(predefinedErrorResultId));
    assertTrue(ldmClientCentraxx.getError(predefinedErrorResultLocation) instanceof Error);
  }

  @Test
  public void testGetVersionString() throws Exception {
    assertFalse(LdmClientUtil.isNullOrEmpty(ldmClientCentraxx.getVersionString()));
  }

  private String readXmlFromFile(String filename) {
    String xml = "";

    try (InputStream is = getClass().getClassLoader().getResourceAsStream(filename)) {
      xml = IOUtils.toString(is, StandardCharsets.UTF_8);
    } catch (IOException ioEx) {
      ioEx.printStackTrace();
    }

    return xml;
  }

  private View xmlToView(String xml) throws JAXBException {
    InputSource inputSource = new InputSource(new StringReader(xml));

    JAXBContext jaxbContext = JAXBContext.newInstance(View.class);
    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
    return (View) jaxbUnmarshaller.unmarshal(inputSource);
  }

}
